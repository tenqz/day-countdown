import Vue from 'vue';

const CountDownApp = () => import('./Components/CountDownApp');

new Vue({
	el: '#mainApp',
	render: h => h(CountDownApp)
});