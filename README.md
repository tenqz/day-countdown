# Fixture Calendar Demo

## Requirements

- Docker
- Docker Compose

## Quick Start

```sh
# Up
docker-compose up -d
docker-compose exec app ash -c "composer install"
docker-compose exec app ash -c "yarn install"
docker-compose exec app ash -c "yarn build"

# Down
docker-compose down
```
