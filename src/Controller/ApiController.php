<?php

namespace App\Controller;

use App\Manager\DateManager;
use App\Manager\ImageManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 *  @Route(path="/api", name="api_")
 */
class ApiController extends AbstractController
{
	/**
	 * @var DateManager
	 */
	private $dateManager;

	/**
	 * @var ImageManager
	 */
	private $imageManager;

	/**
	 * @param DateManager  $dateManager
	 * @param ImageManager $imageManager
	 */
	public function __construct(DateManager $dateManager, ImageManager $imageManager)
	{
		$this->dateManager = $dateManager;
		$this->imageManager = $imageManager;
	}

	/**
	 * @Route(path="/date", name="date", methods={"GET"})
	 *
	 * @return JsonResponse
	 */
	public function date(): JsonResponse
	{
		return $this->json(
			$this->dateManager->getDate()
		);
	}

	/**
	 * @Route(path="/image", name="image", methods={"GET"})
	 *
	 * @return JsonResponse
	 */
	public function image(): JsonResponse
	{
		return $this->json(
			$this->imageManager->getImage()
		);
	}
}
