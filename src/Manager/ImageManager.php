<?php

namespace App\Manager;

use App\Contract\ImageRepositoryInterface;

class ImageManager
{
	/**
	 * @var ImageRepositoryInterface
	 */
	private $repository;

	/**
	 * @param ImageRepositoryInterface $repository
	 */
	public function __construct(ImageRepositoryInterface $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * @return string
	 */
	public function getImage(): string
	{
		return $this->repository->load();
	}
}
