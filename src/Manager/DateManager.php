<?php

namespace App\Manager;

use App\Contract\DateRepositoryInterface;

class DateManager
{
	/**
	 * @var DateRepositoryInterface
	 */
	private $repository;

	/**
	 * @param DateRepositoryInterface $repository
	 */
	public function __construct(DateRepositoryInterface $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * @return string
	 */
	public function getDate(): string
	{
		return $this->repository->load();
	}
}
