<?php

namespace App\Repository;

use App\Contract\DateRepositoryInterface;

class DateRepository implements DateRepositoryInterface
{
	/**
	 * @return string
	 */
	public function load(): string
	{
		return (new \DateTime('14.06.2022'))->getTimestamp();
	}
}
