<?php

namespace App\Repository;

use App\Contract\ImageRepositoryInterface;
use Symfony\Component\Asset\Packages;

class ImageRepository implements ImageRepositoryInterface
{
	/**
	 * @var Packages
	 */
	private $packages;

	/**
	 * @param Packages $packages
	 */
	public function __construct(Packages $packages)
	{
		$this->packages = $packages;
	}

	/**
	 * @return string
	 */
	public function load(): string
	{
		return $this->packages->getUrl('build/images/photo.jpg');
	}
}


