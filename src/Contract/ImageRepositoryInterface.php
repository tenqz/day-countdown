<?php

namespace App\Contract;

interface ImageRepositoryInterface
{
	/**
	 * @return string
	 */
	public function load(): string;
}