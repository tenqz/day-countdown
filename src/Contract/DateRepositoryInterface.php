<?php

namespace App\Contract;

interface DateRepositoryInterface
{
	/**
	 * @return string
	 */
	public function load(): string;
}